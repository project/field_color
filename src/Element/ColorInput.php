<?php

namespace Drupal\field_color\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\field_color\Component\FieldColorValidation;

/**
 * Provides a Color element.
 *
 * @FormElement("input_color")
 */
class ColorInput extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processTestElement'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderColor'],
        [$class, 'preRenderGroup'],
      ],
      '#element_validate' => [
        [$class, 'validateColor'],
      ],
      '#theme' => 'input_color',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function processTestElement(&$element, FormStateInterface $form_state, &$form) {
    return $element;
  }

  /**
   * Prepares a #type 'color' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderColor(array $element) {
    $element['#attributes']['type'] = 'text';
    $element['#attributes']['class'] = [];
    $element['#attributes']['class'][] = 'input-field-color';
    Element::setAttributes($element, [
      'id', 'name', 'value', 'class', 'size', 'field-color',
    ]);

    $element['#palette'] = preg_replace("/[\r\n]+/", "", $element['#palette']);
    $element['#selectionPalette'] = preg_replace("/[\r\n]+/", "", $element['#selectionPalette']);

    $element['#attached'] = [
      'library' => [
        'field_color/field_color.admin',
      ],
      'drupalSettings' => [
        'size' => (Boolean) $element['#size'],
        'type_' => $element['#type_'],
        'showInput' => (Boolean) $element['#showInput'],
        'showInitial' => (Boolean) $element['#showInitial'],
        'allowEmpty' => (Boolean) $element['#allowEmpty'],
        'showAlpha' => (Boolean) $element['#showAlpha'],
        'disabled' => (Boolean) $element['#disabled'],
        'localStorageKey' => $element['#localStorageKey'],
        'showPalette' => (Boolean) $element['#showPalette'],
        'showButtons' => (Boolean) $element['#showButtons'],
        'showPaletteOnly' => (Boolean) $element['#showPaletteOnly'],
        'togglePaletteOnly' => (Boolean) $element['#togglePaletteOnly'],
        'showSelectionPalette' => (Boolean) $element['#showSelectionPalette'],
        'clickoutFiresChange' => (Boolean) $element['#clickoutFiresChange'],
        'hideAfterPaletteSelect' => (Boolean) $element['#hideAfterPaletteSelect'],
        'containerClassName' => $element['#containerClassName'],
        'replacerClassName' => $element['#replacerClassName'],
        'preferredFormat' => $element['#preferredFormat'],
        'cancelText' => $element['#cancelText'],
        'chooseText' => $element['#chooseText'],
        'togglePaletteMoreText' => $element['#togglePaletteMoreText'],
        'togglePaletteLessText' => $element['#togglePaletteLessText'],
        'clearText' => $element['#clearText'],
        'noColorSelectedText' => $element['#noColorSelectedText'],
        'palette' => json_decode($element['#palette'], TRUE),
        'selectionPalette' => json_decode($element['#selectionPalette']),
        'maxSelectionSize' => $element['#maxSelectionSize'],
        'locale' => $element['#locale'],
      ],
    ];

    return $element;
  }

  /**
   * Form element validation handler for #type 'color'.
   */
  public static function validateColor(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = trim($element['#value']);
    if ($value && !FieldColorValidation::all($value)) {
      $form_state->setError($element, t('Field %name must be a valid color.', ['%name' => empty($element['#title']) ? $element['#parents'][0] : $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      // This should be a string, but allow other scalars since they might be
      // valid input in programmatic form submissions.
      return is_scalar($input) ? (string) $input : '';
    }
    return NULL;
  }

}
